ARG DENO_VERSION=1.36.1

FROM harbor.nirvati.org/library/rust:1.71.1 as builder

WORKDIR /deno

RUN apt update && apt install -y git python3 libglib2.0-dev cmake

ARG DENO_VERSION
RUN git clone https://github.com/denoland/deno --recurse-submodules --depth=1 --branch v${DENO_VERSION} .

ENV V8_FROM_SOURCE=1

RUN cargo build --release

FROM scratch

ARG DENO_VERSION
ENV DENO_VERSION=${DENO_VERSION}

COPY --from=builder /deno/target/release/deno /deno
