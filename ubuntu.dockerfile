ARG DENO_VERSION=1.36.1
ARG BIN_IMAGE=harbor.nirvati.org/library/deno:bin-${DENO_VERSION}


FROM ${BIN_IMAGE} AS bin

FROM harbor.nirvati.org/library/ubuntu:22.04

RUN useradd --uid 1993 --user-group deno \
  && mkdir /deno-dir/ \
  && chown deno:deno /deno-dir/

ENV DENO_DIR /deno-dir/
ENV DENO_INSTALL_ROOT /usr/local

ARG DENO_VERSION
ENV DENO_VERSION=${DENO_VERSION}
COPY --from=bin /deno /usr/bin/deno

RUN apt update && apt install -y tini && apt clean && rm -rf /var/lib/apt/lists/*

COPY ./_entry.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/bin/tini", "--", "docker-entrypoint.sh"]
CMD ["run", "https://deno.land/std/examples/welcome.ts"]
